#!/bin/bash
# Kayden: Install SVO
# Follow instructions here: https://github.com/uzh-rpg/rpg_svo
# For ARM, disable below line if not ARM
export ARM_ARCHITECTURE=True

echo "Start Install SVO ..."

echo "Install Sophus"
git clone https://github.com/strasdat/Sophus.git
cd Sophus
git checkout a621ff
mkdir build
cd build
cmake ..
make
cd ../..

echo "Install Fast"
git clone https://github.com/uzh-rpg/fast.git
cd fast
mkdir build
cd build
cmake ..
make
cd ../..

echo "Install Vikit and SVO"
cd catkin_ws/src
git clone https://github.com/uzh-rpg/rpg_vikit.git
cd ..

catkin_make

echo "Finish Install"
