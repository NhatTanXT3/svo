List of issues during compile and run code

#Compile issues
1. sophus is not defined in vikit_common
Solve: Define Sophus Libraries in vikit_common CMakeLists.txt 

# Create vikit library
# @Kay: issue https://github.com/uzh-rpg/rpg_svo/issues/21
SET(Sophus_LIBRARIES ~/ros_ws/svo1/svo/Sophus/build/libSophus.so)
